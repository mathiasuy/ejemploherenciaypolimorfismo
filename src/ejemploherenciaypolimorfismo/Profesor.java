/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploherenciaypolimorfismo;

/**
 *
 * @author mathias-linux
 */
public class Profesor extends Persona {
    private String curso;
    
    public String getCurso(){
        return curso;
    }
    public void setCurso(String curso){
        this.curso=curso;
    }
    
    Profesor(){};
    Profesor(String nombre, int Edad, String curso){
        super(nombre, Edad);
        this.curso = curso;
    }
    Profesor(Persona p, String curso){
        super(p);
        this.curso=curso;
    }
    Profesor(Profesor p){
        super(p.getNombre(),p.getEdad());
        this.curso = p.curso;
    }
    
    @Override
    public String toString(){
        return super.toString() + curso;
    }
}
